<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");

$json_params = file_get_contents('php://input');
$car = json_decode($json_params);

require "connection.php";

$updateStatement = $conn->prepare('UPDATE `cars` SET `car_type` = :car_type, `points_follow` = :points_follow, `speed` = :speed WHERE `id` = :id');
$updateStatement->execute([
    'car_type' => $car->car_type,
    'points_follow' => $car->points_follow,
    'speed' => $car->speed,
    'id'=> $car->id
]);

$selectStatement = $conn->prepare('SELECT * FROM `cars` WHERE id = :id');
$selectStatement->execute([
    'id' => $car->id
]);

$dbCar = $selectStatement->fetch(PDO::FETCH_OBJ);

echo json_encode($dbCar);