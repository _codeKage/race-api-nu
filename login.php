<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");

$json_params = file_get_contents('php://input');
$user = json_decode($json_params);

require "connection.php";

$loginStatement = $conn->prepare('SELECT * FROM `users` WHERE email = :email');
$loginStatement->execute([
    'email' => $user->email
]);

$selectedUser = $loginStatement->fetch(PDO::FETCH_OBJ);

if (!$selectedUser) {
    http_response_code(404);
    echo json_encode('User does not exist');
} else {
    if (!password_verify($user->password, $selectedUser->password)) {
        http_response_code(422);
        echo json_encode('Password does not match');
    } else {
        unset($selectedUser->password);
        echo json_encode($selectedUser);
    }
}