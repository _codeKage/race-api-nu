<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");

$json_params = file_get_contents('php://input');
$user = json_decode($json_params);

require "connection.php";

$password = password_hash($user->password, PASSWORD_BCRYPT);
$createdAt = (new \DateTime())->format('Y-m-d H:i:s');

$insertStatement = $conn->prepare('INSERT INTO `users`(`name`, `email`, `password`, `api_token`, `created_at`) VALUES (:name, :email, :password, :api_token, :created_at)');
$insertStatement->execute([
    'name' => $user->name,
    'email' => $user->email,
    'password' => $password,
    'api_token' => '',
    'created_at' => $createdAt
]);

$userId = $conn->lastInsertId();

$selectStatement = $conn->prepare('SELECT `name`,`email`,`created_at` FROM `users` WHERE id = :id');
$selectStatement->execute([
    'id' => $userId
]);

$createdUser = $selectStatement->fetch(PDO::FETCH_OBJ);

print_r(json_encode($createdUser));