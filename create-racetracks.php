<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");

$json_params = file_get_contents('php://input');
$raceTrack = json_decode($json_params);

require "connection.php";

$createdAt = (new \DateTime())->format('Y-m-d H:i:s');

$createStatement = $conn->prepare('INSERT INTO `race_tracks`(`name`, `description`, `tiles_coords`, `user_id`, `created_at`) VALUES(:name, :description, :tiles_coords, :user_id, :created_at)');
$createStatement->execute([
    'name' => $raceTrack->name,
    'description' => $raceTrack->description,
    'tiles_coords' => $raceTrack->tiles_coords,
    'user_id' => $raceTrack->user_id,
    'created_at' => $createdAt
]);

$lastInsertId = $conn->lastInsertId();

$selectStatement = $conn->prepare('SELECT * FROM `race_tracks` WHERE id = :id');
$selectStatement->execute([
    'id' => $lastInsertId
]);

$createdRaceTrack = $selectStatement->fetch(PDO::FETCH_OBJ);

echo json_encode($createdRaceTrack);