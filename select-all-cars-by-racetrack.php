<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");

$raceTrackId = $_GET['raceTrackId'];

require "connection.php";

$selectStatement = $conn->prepare('SELECT * FROM `cars` WHERE race_track_id = :race_track_id');
$selectStatement->execute([
    'race_track_id' => $raceTrackId
]);

$cars = $selectStatement->fetchAll(PDO::FETCH_OBJ);

echo json_encode($cars);