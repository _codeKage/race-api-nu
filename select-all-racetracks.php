<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");

$json_params = file_get_contents('php://input');

require "connection.php";

$selectStatement = $conn->prepare('SELECT * FROM `race_tracks`');
$selectStatement->execute();

$raceTracks = $selectStatement->fetchAll(PDO::FETCH_OBJ);

echo json_encode($raceTracks);