<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");

$userId = $_GET['userId'];

require "connection.php";

$selectStatement = $conn->prepare('SELECT * FROM `race_tracks` WHERE user_id = :user_id');
$selectStatement->execute([
    'user_id' => $userId
]);

$raceTracks = $selectStatement->fetchAll(PDO::FETCH_OBJ);

echo json_encode($raceTracks);