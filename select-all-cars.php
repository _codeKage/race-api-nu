<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");

require "connection.php";

$selectStatement = $conn->prepare('SELECT * FROM `cars`');
$selectStatement->execute();

$cars = $selectStatement->fetchAll(PDO::FETCH_OBJ);

echo json_encode($cars);