<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");

$carId = $_GET['carId'];

require "connection.php";

$deleteStatement = $conn->prepare('DELETE FROM `cars` WHERE id = :id');
$deleteStatement->execute([
    'id' => $carId
]);

http_response_code(200);
