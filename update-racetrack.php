<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");

$json_params = file_get_contents('php://input');
$raceTrack = json_decode($json_params);

require "connection.php";

$updateStatement = $conn->prepare('UPDATE `race_tracks` SET `name` = :name, `description` = :description, `tiles_coords` = :tiles_coords WHERE `id` = :raceTrackId');
$updateStatement->execute([
    'name' => $raceTrack->name,
    'description' => $raceTrack->description,
    'tiles_coords' => $raceTrack->tiles_coords,
    'raceTrackId'=> $raceTrack->id
]);

$selectStatement = $conn->prepare('SELECT * FROM `race_tracks` WHERE id = :id');
$selectStatement->execute([
    'id' => $raceTrack->id
]);

$dbRaceTrack = $selectStatement->fetch(PDO::FETCH_OBJ);

echo json_encode($dbRaceTrack);