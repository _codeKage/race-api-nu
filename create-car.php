<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");

$json_params = file_get_contents('php://input');
$car = json_decode($json_params);

require "connection.php";

$createStatement = $conn->prepare('INSERT INTO `cars`(`car_type`, `points_follow`, `speed`, `race_track_id`) VALUES(:car_type, :points_follow, :speed, :race_track_id)');
$createStatement->execute([
    'car_type' => $car->car_type,
    'points_follow' => $car->points_follow,
    'speed' => $car->speed,
    'race_track_id' => $car->race_track_id
]);

$lastInsertId = $conn->lastInsertId();

$selectStatement = $conn->prepare('SELECT * FROM `cars` WHERE id = :id');
$selectStatement->execute([
    'id' => $lastInsertId
]);

$createdCar = $selectStatement->fetch(PDO::FETCH_OBJ);

echo json_encode($createdCar);