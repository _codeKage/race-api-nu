<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");

$raceTrackId = $_GET['raceTrackId'];

require "connection.php";

$deleteStatement = $conn->prepare('DELETE FROM `race_tracks` WHERE id = :id');
$deleteStatement->execute([
    'id' => $raceTrackId
]);

http_response_code(200);
